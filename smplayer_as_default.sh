#!/bin/sh
### set smplayer as default
grep "^MimeType=" /usr/share/applications/smplayer.desktop | cut -d "=" -f 2 | xargs -d ';' -n 1 | grep -e "^video/" -e "^x-content/video" | xargs -n 1 -I '{}' xdg-mime default smplayer.desktop '{}'
grep "^MimeType=" /usr/share/applications/smplayer.desktop | cut -d "=" -f 2 | xargs -d ';' -n 1 | grep -e "^audio/" -e "^x-content/audio" | xargs -n 1 -I '{}' xdg-mime default smplayer.desktop '{}'
sudo update-desktop-database
