#!/bin/sh

### apache
pacman -S apache mysql php php-apache phpmyadmin
mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql

# nano /etc/php/php.ini
# extension=mysqli
text=extension=mysqli
sed -i "/^;$text/ c$text" /etc/php/php.ini

echo '
Alias /phpmyadmin "/usr/share/webapps/phpMyAdmin"
<Directory "/usr/share/webapps/phpMyAdmin">
DirectoryIndex index.php
AllowOverride All
Options FollowSymlinks
Require all granted
</Directory>' > /etc/httpd/conf/extra/phpmyadmin.conf

file=/etc/httpd/conf/httpd.conf
# nano $file
# comment 2 strings, uncomment 1
# LoadModule unique_id_module modules/mod_unique_id.so
# LoadModule mpm_event_module modules/mod_mpm_event.so
# #LoadModule mpm_prefork_module modules/mod_mpm_prefork.so
sed -i -e '/LoadModule unique_id_module modules\/mod_unique_id.so/s/^/#/g' \
       -e '/LoadModule mpm_event_module modules\/mod_mpm_event.so/s/^/#/g' \
       -e '/LoadModule mpm_prefork_module modules\/mod_mpm_prefork.so/s/^#//g' $file

if grep -Fxq "LoadModule php7_module modules/libphp7.so" $file
then
  echo "php7_module: already exists in $file"
else
  echo '
LoadModule php7_module modules/libphp7.so
AddHandler php7-script php
Include conf/extra/php7_module.conf
Include conf/extra/phpmyadmin.conf' >> $file
fi

systemctl enable httpd
systemctl restart httpd
systemctl enable mysqld
systemctl restart mysqld
mysql_secure_installation

file=/srv/http/index.html
if [ ! -f $file ]; then
echo "create file $file"
echo '<html><head><title>Welcome</title></head><body><h2>Welcome to my Web Server test page</h2></body></html>' > $file
fi

# /home/bayar/public_html
usermod -a -G bayar http
chmod +755 /home/bayar
