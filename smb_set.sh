#!/bin/bash
# Samba - расшарить папки для локальной сети windows

if [ $(id -u) != "0" ]
  then echo "надо бы запустить как root" >&2; exit 1
fi

# общая папка
guestdir='/mnt/sklad/tmp'
# папка для конфигов расшаривания папок по правой кнопке мыши в файл мэнэджерах
usershar='/var/lib/samba/usershares'
# общий пользователь 1
grupname1='sambagrup'
username1='sambauser'
# сделать: взять имя из папки /home/* или ещё как то
username2='bayar'

#строка "client max protocol = NT1" что то там против обхода windows 10
#homes - заходить под своим именем но другим паролем, в данном случае: bayar 12334
#public - заходить без пароля
echo "# самопальный smb.conf
[global]
client max protocol = NT1
usershare path = $usershar
usershare max shares = 10
[homes]
comment = Домашние папки
valid users = %S
read only = No
browseable = No
[Downloads]
comment = Загрузки
path = /mnt/sklad/Downloads
force user = $username1
force group = $grupname1
read only = Yes
[public]
comment = Общая папка
path = $guestdir
force user = $username1
force group = $grupname1
read only = No
" > /etc/samba/smb.conf

# создать пользователя 1 в системе
#-M, --no-create-home          не создавать домашний каталог пользователя
#-s, --shell ОБОЛОЧКА          регистрационная оболочка новой учётной записи
useradd -M -s /sbin/nologin $username1
(echo "1"; sleep 1; echo "1";) | passwd $username1

# добавить пользователя 1 в samba
#-s                   use stdin for password prompt
#-a                   add user
(echo "1"; sleep 1; echo "1" ) | smbpasswd -s -a $username1
# добавить пользователя 2 в samba
(echo "12334"; sleep 1; echo "12334" ) | smbpasswd -s -a $username2

# добавить группу в систему, добавить пользователей в эту группу
#-a, --append                  добавить пользователя в дополнительные
#                              ГРУППЫ, указанные в параметре -G не удаляя
#                              пользователя из других групп
groupadd $grupname1
usermod -aG $grupname1 $username1
usermod -aG $grupname1 $username2

# создать общую папку
#-p, --parents      не выдавать ошибку, если существует, создавать
#                   родительские каталоги, если необходимо
mkdir -p $guestdir

# дать доступ к папке для группы пользователей
#-R, --recursive        рекурсивно обрабатывать файлы и каталоги
chgrp $grupname1 $guestdir

chmod 2775 $guestdir

# папка для расшаривания по правой кнопке мыши
mkdir $usershar
chgrp $grupname1 $usershar
chmod 1770 $usershar

# при первом запуске включить и запустить
systemctl enable --now smb nmb
# при втором запуске перезапустить
#systemctl restart smb nmb
